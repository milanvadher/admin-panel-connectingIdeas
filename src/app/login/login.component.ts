import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { DatabaseServiceService } from '../database-service.service';
import { storage } from 'firebase';
import { LoadingService } from '../loading.service';


export class User {
  email: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user: User = new User();
  price = 50;

  constructor(
    public db: DatabaseServiceService,
    private elementRef: ElementRef,
    public router: Router,
    public loading: LoadingService
  ) { }

  ngOnInit() {
    if (localStorage.getItem('Login')) {
      this.router.navigateByUrl('/home');
    }
  }

  login() {
    this.db.login(this.user.email + '@saffrony.ac.in', this.user.password);
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    // this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(98, 192, 192)';
 }

}
