import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { DatabaseServiceService } from './database-service.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor( private authService: DatabaseServiceService, private router: Router ) {
  }

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ) {
    if (localStorage.getItem('Login')) {
    // if (this.authService.isAuthenticate === true) {
      return true;
    } else {
      this.router.navigateByUrl('/login');
    }
}
}
