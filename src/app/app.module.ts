import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// tslint:disable-next-line:max-line-length
import { MatButtonModule, MatCheckboxModule, MatToolbarModule, MatProgressSpinnerModule, MatFormFieldModule, MatCardModule, MatInputModule, MatRadioModule, MatIconModule, MatDialogModule, MatDialogActions, MatDialogContent, MatMenuModule, MatProgressBarModule, MatGridListModule } from '@angular/material';
import { AppComponent } from './app.component';
import { TitlebarComponent } from './titlebar/titlebar.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AngularFireModule } from 'angularfire2';
import { SignupComponent } from './signup/signup.component';
import { AngularFireAuth, AngularFireAuthModule } from 'angularfire2/auth';
import { FormsModule } from '@angular/forms';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { LoadingService } from '../app/loading.service';
import { HttpClientModule } from '@angular/common/http';
import { DatabaseServiceService } from '../app/database-service.service';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { DialogComponent } from './dialog/dialog.component';
import { AuthGuard } from './auth.guard';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProfileComponent } from './profile/profile.component';
import { AngularFireDatabase } from 'angularfire2/database-deprecated';
import { ReversePipe } from './reverse.pipe';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { LoaderComponent } from './loader/loader.component';

export const firebaseConfig = {
  apiKey: 'AIzaSyD-3smA3LPFl_UJ2MpcjCnhKSRIm7dj3Ms',
  authDomain: 'saffrony-524ca.firebaseapp.com',
  databaseURL: 'https://saffrony-524ca.firebaseio.com',
  projectId: 'saffrony-524ca',
  storageBucket: 'saffrony-524ca.appspot.com',
  messagingSenderId: '235588142857'
};

@NgModule({
  declarations: [
    AppComponent,
    TitlebarComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    DialogComponent,
    PageNotFoundComponent,
    ProfileComponent,
    ReversePipe,
    LoaderComponent,
  ],
  imports: [
    Ng2ImgMaxModule,
    FormsModule,
    HttpClientModule,
    BrowserModule,
    MatGridListModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatDialogModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatCardModule,
    MatMenuModule,
    MatProgressBarModule,
    AngularFireStorageModule,
    MatInputModule,
    MatIconModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    AngularFirestoreModule.enablePersistence(),
    RouterModule.forRoot([
      { path: '', component: LoginComponent },
      { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
      { path: 'login', component: LoginComponent },
      { path: 'signup', component: SignupComponent },
      { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
      { path: 'pnf', component: PageNotFoundComponent },
      { path: '**', redirectTo: 'pnf' }
    ])
  ],
  entryComponents: [
    DialogComponent
  ],
  providers: [
    AngularFireAuth,
    DatabaseServiceService,
    AuthGuard,
    AngularFireDatabase,
    LoadingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
