import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { User } from '@firebase/auth-types';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { DialogComponent } from './dialog/dialog.component';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { LoadingService } from './loading.service';

@Injectable()
export class DatabaseServiceService {

  uniqkey: any;
  userData: any;
  dialogResult: any;
  userProfile: firebase.database.Reference;
  currentUser: firebase.User;
  isAuthenticate: Boolean = false;

  constructor(
    public fAuth: AngularFireAuth,
    public router: Router,
    private firestore: AngularFirestore,
    public dialog: MatDialog,
    public loaderService: LoadingService
  ) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.currentUser = user;
        this.userProfile = firebase.database().ref(`/userProfile/${user.uid}/userData/`);
      }
    });
  }

  updateProfilePic(img) {
    this.userProfile.update({
      profilePic: img
    });
  }

  login(email, password) {
    this.showLoader();
    this.fAuth.auth.signInWithEmailAndPassword(email, password)
      .then(() => {
        this.currentUser = firebase.auth().currentUser;
        if (this.currentUser.emailVerified === true) {
          const token = localStorage.setItem('Login', 'done');
          this.isAuthenticate = true;
          this.router.navigateByUrl('/home');
          this.onEnd();
        } else {
          const dialogRef = this.dialog.open(DialogComponent, {
            width: '600px',
            data: 'Your E-mail is not verified please verify your mail.'
          });
        }
      })
      .catch(err => {
        console.log('Error : ', err);
      });
  }

  private onEnd(): void {
    this.hideLoader();
}

  private showLoader(): void {
    this.loaderService.show();
  }

  private hideLoader(): void {
    this.loaderService.hide();
  }

  images(imgUrl) {
    this.userProfile = firebase.database().ref(`/userProfile/${this.currentUser.uid}/userData`);
    this.userProfile.on('value', userProfileData => {
      this.userData = userProfileData.val();
      this.uniqkey = Math.floor(Date.now() / 1000);
      firebase.database().ref('moderator/').child(this.uniqkey).set({
        id: this.currentUser.uid,
        fname: this.userData.firstname,
        lname: this.userData.lastname,
        profilePic: this.userData.profilePic,
        likes: 0,
        feed: imgUrl
      });
    });
  }

  getProfile() {
    this.userProfile = firebase.database().ref(`/userProfile/${this.currentUser.uid}/`);
    this.userProfile.on('value', this.gotData, this.errData);
  }

  gotData(data) {
    console.log(data.val());
  }

  errData(err) {
    console.log(err);
  }

  async register(email, fname, lname, mobile, gender, password, repassword, profilepic): Promise<User> {
    try {
      const newUser: User = await this.fAuth.auth.createUserWithEmailAndPassword(email + '@saffrony.ac.in', password);

      await newUser.sendEmailVerification().then(() => {
        const dialogRef = this.dialog.open(DialogComponent, {
          width: '600px',
          data: 'We send email to verify your mail-id'
        });
      }).then(() => {
        firebase.database().ref('/userProfile/').child(newUser.uid).child('/userData/').set({
          id: newUser.uid,
          email: email + '@saffrony.ac.in',
          firstname: fname,
          lastname: lname,
          mobile: mobile,
          gender: gender,
          profilePic: profilepic
        });
      }).then(() => {
        this.router.navigateByUrl('/home');
      });
      return newUser;
    } catch (error) {
      console.error(error);
      throw new Error();
    }
  }

  getUserProfile() {
    console.log(this.currentUser.emailVerified);
  }

  logoutUser() {
    return this.fAuth.auth.signOut().then(() => {
      localStorage.clear();
      this.router.navigateByUrl('/login');
    });
  }

}
