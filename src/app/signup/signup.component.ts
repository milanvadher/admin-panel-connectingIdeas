import { Component, OnInit, ElementRef } from '@angular/core';
import { DatabaseServiceService } from '../database-service.service';
import { AngularFireStorage } from 'angularfire2/storage';
import { Router } from '@angular/router';

export class User {
  fname: string;
  lname: string;
  mobile: number;
  gender: string;
  email: string;
  password: string;
  repassword: string;
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public user: User = new User();
  // tslint:disable-next-line:max-line-length
  profilePic = 'https://firebasestorage.googleapis.com/v0/b/saffrony-524ca.appspot.com/o/App%20Images%2FXTIp2czY_400x400.jpg?alt=media&token=04fcfd9f-de15-4b82-81e8-66f8bfc7ce82';

  constructor(
    private elementRef: ElementRef,
    public db: DatabaseServiceService,
    public router: Router
  ) { }

  ngOnInit() {
    if (localStorage.getItem('Login')) {
      this.router.navigateByUrl('/home');
    }
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    // this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(247, 197, 89)';
 }

  register() {
    // tslint:disable-next-line:max-line-length
    this.db.register(this.user.email, this.user.fname, this.user.lname, this.user.mobile, this.user.gender, this.user.password, this.user.repassword, this.profilePic);
  }

  profile() {

  }

}
