import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { LoaderState } from './loader/loader';

@Injectable()
export class LoadingService {

  private loaderSubject = new Subject<LoaderState>();

  loaderState = this.loaderSubject.asObservable();

  constructor() { }

  show() {
    console.log('showLoader');
    this.loaderSubject.next(<LoaderState>{ show: true });
  }

  hide() {
    console.log('hideLoader');
    this.loaderSubject.next(<LoaderState>{ show: false });
  }

}
