import { Component, OnInit, Inject } from '@angular/core';
import { DatabaseServiceService } from '../database-service.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';
import { AngularFireStorage } from 'angularfire2/storage';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { ReversePipe } from '../reverse.pipe';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { DomSanitizer } from '@angular/platform-browser';
import { DatabaseReference, AngularFireList } from 'angularfire2/database';
import { FirebaseDatabase } from '@firebase/database-types';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  uid: any;
  userData: any;
  image: any;
  firebaseList: FirebaseListObservable<any[]>;
  captureDataUrl: File;
  userFeeds: any;
  dialogResult: any;
  items: FirebaseListObservable<any[]>;
  // items;
  title = 'app';
  selectedFiles: FileList;
  file: File;
  imgsrc;
  color = 'primary';
  mode: 'determinate';
  progressBarValue;

  uploadedImage: File;
  imagePreview: any;

  public userProfile: firebase.database.Reference;
  public currentUser: firebase.User;
  public data: AngularFireList<any>;

  constructor(private db: DatabaseServiceService,
    public dialog: MatDialog,
    public fAuth: AngularFireAuth,
    private storage: AngularFireStorage,
    public af: AngularFireDatabase,
    private ng2ImgMax: Ng2ImgMaxService,
    public sanitizer: DomSanitizer
  ) {
    this.items = af.list('/moderator');
  }

  other() {
    // this.data = this.af.list('', ref => ref)
  }


  ngOnInit() {
  }

  onImageChange(event) {
    this.image = event.target.files[0];
  }

  getImagePreview(file: File) {
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imagePreview = reader.result;
    };
  }

  chooseFiles(event) {
    this.selectedFiles = event.target.files;
  }

  like($key, like) {
    this.items.update($key, { likes: like + 1 });
  }

  uploadIg() {
    if (this.selectedFiles.item(0)) {
      this.uploadIpic();
    }
  }

  // uploadIg() {
  //   this.ng2ImgMax.compressImage(this.image, 0.400).subscribe(
  //     result => {
  //       this.uploadedImage = new File([result], result.name);
  //       this.getImagePreview(this.uploadedImage);

  //       const file = this.uploadedImage;
  //       const uniqkey = 'pic' + Math.floor(Math.random() * 1000000);
  //       const uploadTask = this.storage.upload('moderator/' + uniqkey, file);

  //       this.imgsrc = uploadTask.downloadURL().subscribe((img) => {
  //         this.db.images(img);
  //       });
  //       console.log(this.imgsrc);

  //       uploadTask.percentageChanges().subscribe((value) => {
  //         this.progressBarValue = value.toFixed(2);
  //       });

  //     },
  //     error => {
  //       console.log('😢 Oh no!', error);
  //     }
  //   );
  // }


  uploadIpic() {
    const file = this.selectedFiles.item(0);
    const uniqkey = 'pic' + Math.floor(Math.random() * 1000000);
    const uploadTask = this.storage.upload('moderator/' + uniqkey, file);

    this.imgsrc = uploadTask.downloadURL().subscribe((img) => {
      this.db.images(img);
    });
    console.log(this.imgsrc);

    uploadTask.percentageChanges().subscribe((value) => {
      this.progressBarValue = value.toFixed(2);
    });
  }

  uploadPP() {
    if (this.selectedFiles.item(0)) {
      this.uploadpic();
    }
  }

  uploadpic() {
    const file = this.selectedFiles.item(0);
    const uniqkey = 'pic' + Math.floor(Math.random() * 1000000);
    const uploadTask = this.storage.upload('/angfire2store/' + uniqkey, file);

    this.imgsrc = uploadTask.downloadURL().subscribe((img) => {
      this.db.updateProfilePic(img);
    });
    console.log(this.imgsrc);

    uploadTask.percentageChanges().subscribe((value) => {
      this.progressBarValue = value.toFixed(2);
    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '600px',
      data: 'This text is passed into the dialog'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog closed: ${result}`);
      this.dialogResult = result;
    });
  }

}
