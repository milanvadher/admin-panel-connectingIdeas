import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatabaseServiceService } from '../database-service.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-titlebar',
  templateUrl: './titlebar.component.html',
  styleUrls: ['./titlebar.component.css']
})
export class TitlebarComponent implements OnInit {

  public userProfile: firebase.database.Reference;
  public currentUser: firebase.User;
  userData: any;
  imgsrc = 'https://material.angular.io/assets/img/examples/shiba1.jpg';

  constructor(public router: Router, public db: DatabaseServiceService) { }

  ngOnInit() {
    this.profile();
  }

  profile() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.currentUser = user;
        this.userProfile = firebase.database().ref(`/userProfile/${user.uid}/userData`);
        this.userProfile.on('value', userProfileData => {
          this.userData = userProfileData.val();
        });
      }
    });
  }

  viewProfile() {
    console.log('Enter Profile page !!!');
    this.router.navigateByUrl('/profile');
  }

  home() {
    console.log('Enter home !!!');
    this.router.navigateByUrl('/home');
  }

  login() {
    console.log('Enter Login !!!');
    this.router.navigateByUrl('/login');
  }

  signup() {
    this.router.navigateByUrl('/signup');
  }

  logout() {
    this.db.logoutUser();
  }

}
