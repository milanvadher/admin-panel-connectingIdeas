import { Component, OnInit, ElementRef } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFireStorage } from 'angularfire2/storage';
import { DatabaseServiceService } from '../database-service.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public userProfile: firebase.database.Reference;
  private elementRef: ElementRef;
  public currentUser: firebase.User;
  userData: any;
  color = 'warn';
  mode: 'determinate';

  selectedFiles: FileList;
  imgsrc;
  progressBarValue;
  file: File;

  constructor(
    private storage: AngularFireStorage,
    private db: DatabaseServiceService
  ) { }

  ngOnInit() {
    this.profile();
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    // this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(244, 244, 244)';
  }

  chooseFiles(event) {
    this.selectedFiles = event.target.files;
  }

  uploadPP() {
    if (this.selectedFiles.item(0)) {
      this.uploadpic();
    }
  }

  uploadpic() {
    const file = this.selectedFiles.item(0);
    const uniqkey = 'pic' + Math.floor(Math.random() * 1000000);
    const uploadTask = this.storage.upload('/angfire2store/' + uniqkey, file);

    this.imgsrc = uploadTask.downloadURL().subscribe((img) => {
      this.db.updateProfilePic(img);
    });
    console.log(this.imgsrc);


    uploadTask.percentageChanges().subscribe((value) => {
      this.progressBarValue = value.toFixed(2);
    });
  }

  profile() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.currentUser = user;
        this.userProfile = firebase.database().ref(`/userProfile/${user.uid}/userData`);
        this.userProfile.on('value', userProfileData => {
          this.userData = userProfileData.val();
        });
      }
    });
  }

}
